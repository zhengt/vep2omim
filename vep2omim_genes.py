#!usr/bin/python2

from __future__ import print_function
import sys
import os
import datetime
import plotly
from plotly.offline import plot
import plotly.graph_objs as go

"""compare gene/transcript list between refseqs_for_sophia_analysis.csv and nirvana_gene2transcript"""

def compare_gene_in_vep2omim(vep, omim):

	dir_path = os.path.dirname(vep)
	output = os.path.join(dir_path, "gene_vep2omim_log.txt")

	vep_genes = []
	omim_genes = []
	in_out = []

	with open (vep, "r") as vep_file:
		for v_index, line in enumerate(vep_file):
			v_fields = line.strip().split("\t")
			if line.startswith("#"):
				v_column_num = v_fields.index("SYMBOL")
				continue
			elif v_index > 0 and v_fields[v_column_num].isalnum():
				vep_genes.append(v_fields[v_column_num])

	sorted_set_vep_genes = sorted(set(vep_genes))

	with open (omim, "r") as omim_file:
		for m_index, line in enumerate(omim_file):
			m_fields = line.strip().split("\t")
			if line.startswith("#") and m_index == 4:
				m_column_num = m_fields.index("Approved Gene Symbol (HGNC)")
				continue
			elif m_index > 4 and len(m_fields) > 3 and m_fields[m_column_num].isalnum():
				omim_genes.append(m_fields[m_column_num])
			else:
				continue

	sorted_set_omim_genes = sorted(set(omim_genes))

	with open(output, "a") as log_file:
		log_file.writelines("vep_annotated_file: {}\n".format(vep))

	for gene in sorted_set_vep_genes:
		if gene in sorted_set_omim_genes:
			in_out.append("Y")
			print ("{}\tpresent in OMIM database".format(gene))
			with open(output, "a") as log_file:
				log_file.writelines("{}\t present in OMIM\n".format(gene))
		else:
			in_out.append("N")
			print ("{}\tabsent in OMIM database".format(gene))
			with open(output, "a") as log_file:
				log_file.writelines("{}\t absent in OMIM\n".format(gene))

	print ("{}".format(datetime.datetime.today()))
	with open(output, "a") as log_file:
		log_file.writelines("{}\n".format(datetime.datetime.today()))

	return sorted_set_vep_genes, in_out

def main(vep, omim):

	vep_genes, in_out_omim = compare_gene_in_vep2omim(vep, omim)
	
	# use plotly to create a table showing all the genes, and whether the gene is present in OMIM database.
    
	trace = go.Table(
		columnwidth = [40] + [40],
		columnorder = [0, 1],
	    header=dict(values=['genes_from_VEP_annotated_txt', 'present_in_OMIM?'],
	                line = dict(color='#000000'),
	                font = dict(color=['rgb(45, 45, 45)'] * 5, size=14),
	                fill = dict(color='#E6E6FA'),
	                align = ['left'] * 5),
	    cells=dict(values=[[gene for gene in vep_genes], [status for status in in_out_omim]],
	               line = dict(color='#000000'),
	               fill = dict(color='#FFFFFF'),
	               align = ['left'] * 5))

	layout = dict(
		width=950,
		height=800,
		autosize=True,
		title="VEP_annotated_vcf_genes compared to OMIM in {}".format(vep),
	)
	data = [trace]
	fig = dict(data=data, layout=layout)
	plot(fig, filename ="vep2omim_table.html")
	
if __name__ == "__main__":
	main(sys.argv[1], sys.argv[2])










