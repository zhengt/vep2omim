# CBI-6 DOP1 Comparing genes from VEP annotated vcf and OMIM database
 - vep2omim_genes.py
 
By Tengyue Zheng
13/12/2018

## Function:

Enable the user to know which genes from VEP annotated VCF are present in the OMIM database Python 2.7.5

The test VCF file is upon request by email @ tengyue.zheng@addenbrookes.nhs.uk

## User Requirements:
- display the genes in a VEP annotated vcf, and show whether that gene is present/absent in OMIM database.

## Instructions:

Before running the program, please read the following instructions:

1.Navigate to your working directory. 

2.If possible create a virtual environment, so that you can control the library package available for use. To run the script without testing, standalone python 2.7.5 should be sufficient. If you wish to run in python 3,please feel free to alter the script after you have cloned the repository in your local workng directory to test the script in python 3 and above.

a) check you have virtualenv installed
```Bash
$pip freeze
plotly==3.3.0
```
if you do not have virtualenv, type 
```Bash 
$pip install virutalenv
```
```Bash
$virtualenv --version
```
check the virutalenv version is 1.10.1 or later

b) create a new virtualenv called "env"
```Bash
$virtualenv env
```

c) activate the virtual environment
```Bash
$ source env/bin/activate
```

d) check you have git installed
```Bash
$ git --version
git version 1.8.3.1
```

3.Clone the repository using git
```Bash
git clone https://github.com/tz2614/vep2omim.git
or 
git clone https://git.ctrulab.uk/zhengt/vep2omim.git 
```

- This should create directory called "vep2omim" in your working directory.
- check to make sure the main script called "vep2omim.py" is present
- If you know where you vcf file is located, then try to use the full path to the file if possible.

4.Navigate to the working directory, type the following on command line
(for purpose demonstration I will use "vep_annotated_vcf.txt" as the text file downloaded from VEP, and "mim2gene.txt" as the OMIM database containing the genes of interest.)

```Bash
$python2 md5sumscript.py vep_annotated_vcf.txt mim2gene.txt
```

5.The outputs should look like this:

```
COL3A1  present in OMIM database
DXO     present in OMIM database
EFEMP2  present in OMIM database
FBN1    present in OMIM database
FBN2    present in OMIM database
MIR1236 absent in OMIM database
MIR4673 absent in OMIM database
MUS81   present in OMIM database
MYH11   present in OMIM database
MYLK    present in OMIM database
NDE1    present in OMIM database
NELFE   present in OMIM database
NOTCH1  present in OMIM database
SKIV2L  present in OMIM database
SMAD3   present in OMIM database
STK19   present in OMIM database
TGFBR2  present in OMIM database
2018-12-13 11:01:27.331320
```

After the program is complete, a results table of all the genes from the vep_annotated_vcf.txt are created in our working directory, called "vep2omim_table.html". An example of this can be seen here

VEP_annotated_vcf_genes compared to OMIM in vep_annotated_vcf.txt

| genes_from_VEP_annotated_txt		| present_in_OMIM?			|
|:--------------------------------- |:------------------------- |
| COL3A1  							| Y 						|
| DXO							 	| Y 						|
| EFEMP2						 	| Y 						|

## Output explanation and interpretation

- From line 1 to 17, the first column of text e.g. "COL3A1" shows all the genes present in the vep_annotated_vcf.txt, the second column "present in OMIM database" shows whether the gene name is present OMIM or not.

- On line 18, the date and time of the check are recorded.

- The "vep2omim_table.html" displays the name of the text file downloaded from VEP in the title. The table is separated into 5 columns. The first column in the table contain all the genes present in the vcf file uploaded to VEP. The second contains status of the gene in OMIM, indicating whether the gene is present or not in OMIM.